FROM php:7.4-apache

ENV APP_VERSION=1.0.0

WORKDIR /var/www/html

COPY ./app .

RUN docker-php-ext-install pdo pdo_mysql

EXPOSE 80