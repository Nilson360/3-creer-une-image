<?php

/**
 * Fichier de configuration principal
 */

// Connexion MySQL avec PDO
require_once __DIR__ . '/pdo.php';

// Initialisation de la BDD
require_once __DIR__ . '/db_init.php';

// Enregistrement de la visite d'une page
require_once __DIR__ . '/save_visit.php';