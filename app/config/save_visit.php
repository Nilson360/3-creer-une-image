<?php

// Enregistrer la visite d'une page
$query = $con->prepare(
    'INSERT INTO visit (path, visited_at)
    VALUES (:path, NOW())'
);

$query->bindValue('path', $_SERVER['REQUEST_URI']);
$query->execute();
