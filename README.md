# 3 - Créer une image

Le code source de l'application se trouve dans le dossier `/app`. Un fichier `Dockerfile` est fourni (vide).

-----

>**Objectif:** conteneuriser l'application en créant son image `mon-app`.

<br>**1)** L'**image** doit se baser sur `php:7.4-apache`.

<br>**2)** Elle doit enregistrer la version de l'application (version `1.0.0`) dans une variable `APP_VERSION`.

<br>**3)** Copier le contenu de l'application dans le répertoire racine du serveur *Apache*: `/var/www/html`.

<br>**4)** Installer l'extension `pdo_mysql`.

<br>**5)** Exposer le **port** `80`.

<br>

>L'application nécessite un serveur *MySQL* pour fonctionner car elle enregistre les visites de toutes les pages.

<br>**6)** Créer un conteneur `database` d'un *MySQL* v5.7 avec une base de donnée `my_app`.

<br>**7)** Créer un conteneur `app` de l'image `mon-app` créée précédemment. Le serveur web doit être accessible sur la machine hôte et les variables d'environnement suivantes doivent être spécifiées:

 - `DB_HOST`: l'adresse du serveur *MySQL*
 - `DB_PORT`: le port à utiliser sur le serveur
 - `DB_NAME`: le nom de la base de données à utiliser
 - `DB_USER`: l'utilisateur *MySQL*
 - `DB_PASS`: le mot de passe *MySQL*

<br>**8)** Créer un *réseau* Docker `tp-network` et connecter les conteneurs `app` & `database`.

<br>
Accéder à l'application pour vérifier le fonctionnement.